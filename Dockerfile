# build stage-1 

FROM node:10.15.2
WORKDIR /usr/src/app

COPY package*.json ./
COPY tsconfig.json ./

RUN npm install 

# RUN npm install pm2 -g

COPY ./src ./src

RUN npm run build
# COPY ./build .


# build stage-2

FROM node:10.15.2-alpine

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --only=production
COPY --from=0 /usr/src/app/build ./build
EXPOSE 4000

# CMD ["pm2-runtime", "app.js"]
CMD ["npm", "start"]
