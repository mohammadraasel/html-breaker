import express,{ Application, Request, Response, NextFunction } from 'express';
import bodyParser from 'body-parser';

import Routes from './routes'
import Connect from './connect'

const app : Application = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.get('/', (req: Request, res: Response) => {

    res.send('TS App is Running')
})




const PORT = process.env.PORT || 4000;

const db = process.env.MONGODB_URL || "";


Connect({ db });
Routes({ app })




app.listen(PORT, () => {
    console.log(`server is running on PORT ${PORT}`)
})